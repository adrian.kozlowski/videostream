/**
 * 
 */
package pl.quider.util;

/**
 * Statusy aplikacji dzięki którym wiadomo, czy aplikacja jest właśnie w stanie gotowości 
 * czy też własnie przesyła dane z kamery
 * @author adrian
 *
 */
public enum MediaRecordStates {
	RECORDING,
	STANDBY
	
}
