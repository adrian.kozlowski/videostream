/**
 * 
 */
package pl.quider.util;

/**
 * Stany, które może przyjmować serwer  będące częścią nagłówka odpowiedzi.
 * @author Adrian
 *
 */
public class HeaderStates {
	public static final String CONTINUE = "100 Continue";
	public static final String OK = "200 OK";
	public static final String CREATED = "201 Created";
	public static final String LOW_ON_STORAGE_SPACE = "250 Low on Storage Space";
	public static final String MULTIPLE_CHOICES = "300 Multiple Choices";
	public static final String MOVED_PERMANENTLY = "301 Moved Permanently";
	public static final String MOVED_TEMPORARLY = "302 Moved Temporarily";
	public static final String SEE_OTHER = "303 See Other";
	public static final String NOT_MODIFIED = "304 Not Modified";
	public static final String USE_PROXY = "305 Use Proxy";
	public static final String BAD_REQUEST = "400 Bad Request";
	public static final String UNATHORIZED = "401 Unauthorized";
	public static final String PAYMENT_REQUIRED = "402 Payment Required";
	public static final String FORBIDDEN = "403 Forbidden";
	public static final String NOT_FOUND = "404 Not Found";
	public static final String METCHOD_NOT_ALLOWED = "405 Method Not Allowed";
	public static final String NOT_ACCEPTABLE = "406 Not Acceptable";
	public static final String PROXY_AUTHENTICATION_REQUIDED = "407 Proxy Authentication Required";
	public static final String REQUEST_TIMEOUT = "408 Request Time-out";
	public static final String GONE = "410 Gone";
	public static final String LENGTH_REQUIRED = "411 Length Required";
	public static final String PRECONDIDTION_FAILED = "412 Precondition Failed";
	public static final String REQUEST_ENTITY_TOO_LARGE = "413 Request Entity Too Large";
	public static final String REQUEST_URI_TOO_LARGE = "414 Request-URI Too Large";
	public static final String UNSUPPORTED_MEDIA_TYPE = "415 Unsupported Media Type";
	public static final String PARAMETER_NOT_UNDERSTOOD = "451 Parameter Not Understood";
	public static final String CONFERENCE_NOT_FOUND = "452 Conference Not Found";
	public static final String NOT_ENOUGH_BANDWIDTH = "453 Not Enough Bandwidth";
	public static final String SESSION_NOT_FOUND = "454 Session Not Found";
	public static final String METHOD_NOT_VALID_IN_THIS_STATE = "455 Method Not Valid in This State";
	public static final String HEADER_FIELD_NOT_VALID_FOR_RESOURCE = "456 Header Field Not Valid for Resource";
	public static final String INVALID_RANGE = "457 Invalid Range";
	public static final String PARAMETER_IS_READONLY = "458 Parameter Is Read-Only";
	public static final String AGGREGATE_OPERATION_NOT_ALLOWED = "459 Aggregate operation not allowed";
	public static final String ONLY_AGGREGATE_OPERATION_ALLOWED = "460 Only aggregate operation allowed";
	public static final String UNSUPORTED_TRANSPORT = "461 Unsupported transport";
	public static final String DESTINATION_UNREACHABLE = "462 Destination unreachable";
	public static final String INTERNAL_SERVER_ERROR = "500 Internal Server Error";
	public static final String NOT_IMPLEMENTED = "501 Not Implemented";
	public static final String BAD_GATEWAY = "502 Bad Gateway";
	public static final String SERVICE_UNAVAILABLE = "503 Service Unavailable";
	public static final String GATEWAY_TIIMEOUT = "504 Gateway Time-out";
	public static final String RTSP_VERSION_NOT_SUPPORTED = "505 RTSP Version not supported";
	public static final String OPTION_NOT_SUPPORTED = "551 Option not supported";
	
}
