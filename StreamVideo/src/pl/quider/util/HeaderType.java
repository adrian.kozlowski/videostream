/**
 * 
 */
package pl.quider.util;

/**
 * Typy nagłówków żądań klientów
 * @author Adrian
 *
 */
public enum HeaderType {
		OPTIONS, DESCRIBE, PLAY, TEARDOWN, SETUP, GET_PARAMETER;
}


