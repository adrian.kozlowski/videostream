/**
 * 
 */
package pl.quider.rtsp;

import pl.quider.StreamVideo.stream.Session;

/**
 * Interface obsługujący zdarzenia startu i stopu strumieniowania poprzez serwer rtsp
 * 
 * @author akozlowski
 *
 */
public interface StreamSessionListener {
	/**
	 * Wywołana zawsze gdy sesja rozpocznie strumieniowanie
	 * @param session aktualna sesja
	 */
	public void onStart(Session session);
	
	/**
	 * Wywołana zawsze, gdy sesja zakończy strumieniowanie.
	 * @param session aktualna sesja
	 */
	public void onStop(Session session);
}
