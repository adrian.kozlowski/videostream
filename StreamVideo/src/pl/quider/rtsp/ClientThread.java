/**
 * 
 */
package pl.quider.rtsp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.quider.StreamVideo.properties.Tags;
import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.response.DescribeResponse;
import pl.quider.rtsp.response.GetParameterResponse;
import pl.quider.rtsp.response.NotSupportedResponse;
import pl.quider.rtsp.response.OptionsResponse;
import pl.quider.rtsp.response.PlayResponse;
import pl.quider.rtsp.response.SetupResponse;
import pl.quider.rtsp.response.TeardownResponse;
import pl.quider.util.HeaderStates;
import pl.quider.util.HeaderType;
import android.util.Log;

/**
 * @author Adrian
 * 
 */
public class ClientThread extends Thread implements Runnable, Closeable {
	private BufferedReader br;
	private OutputStream bw;
	
	/**
	 * Gniazdo połączenia
	 */
	private Socket socket;

	/**
	 * Każdemu klientowi przypada jedna sesja.
	 */
	private Session session;

	/**
	 * Uruchomienie konstruktora serwera RTSP jest dowodem na to, że zostało
	 * zaakceptopwane połączenie od klienta. Tworzone są obiekty
	 * {@link BufferedReader} i {@link BufferedWriter} dla wymiany nagłówków
	 * oraz ustawiany {@link Charset} na Cp1250.
	 * 
	 * @param socket
	 *            bierzące połączenie z klientem
	 * @param streamVideoActivity 
	 * @throws IOException
	 */
	public ClientThread(Socket socket, StreamSessionListener streamVideoActivity) throws IOException {
		this.socket = socket;
		br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		bw = this.socket.getOutputStream();
		this.session =  new Session(socket.getLocalAddress(),socket.getInetAddress(),socket.getPort());
		this.session.addListener(streamVideoActivity);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		Log.i(Tags.CLIENT_THREAD, "Zaakceptowano połączenie od " + socket.getInetAddress());
		while (true) {
			Header header = null;
			try {
				header = parseRequest();
			} catch (SocketException e) {
				break;
			} catch (Exception e) {
				Log.e(Tags.CLIENT_THREAD, "Nie można wysać nagówka:" + e.getMessage());
				try {
					Response response = new NotSupportedResponse(header, session);
					response.setStatus(HeaderStates.BAD_REQUEST);
					response.send(bw);
				} catch (IOException e1) {
					Log.w(Tags.CLIENT_THREAD, e1.getMessage());
				}
			}
			try {
				Response response = ResponseFactory.createResponse(header, session);
				if (response != null) {
					Log.d(Tags.CLIENT_THREAD, "Odpowiedź: " + response.toString());
					response.send(bw); // wysyłam odpowiedź
				}
			} catch (Exception e) {
				try {
					Response response = new NotSupportedResponse(header, session);
					response.setStatus(HeaderStates.BAD_REQUEST);
					response.send(bw);
				} catch (IOException e1) {
					Log.w(Tags.CLIENT_THREAD, e1.getMessage());
				}
			}
		}
	}

	/**
	 * Metoda parsująca żądanie klienta.
	 * 
	 * @return {@link Header} nagłówek lub <code>null</code> w razie błędu.
	 * @throws IOException 
	 */
	private Header parseRequest() throws SocketException, IOException {
		final Pattern regexMethod = Pattern.compile("(\\w+) (\\S+) RTSP", Pattern.CASE_INSENSITIVE);
		Header header = new Header();
		String readLine = null;

		if ((readLine = br.readLine()) == null)
			throw new SocketException("Klient rozłączył się");
		System.out.println(readLine);
		Matcher matcher = regexMethod.matcher(readLine);
		if (matcher.find()) {
			String headerType = matcher.group(1);
			for (HeaderType type : HeaderType.values()) {
				if(type.name().equals(headerType))
					header.setType(type);
			}
			header.setUri(matcher.group(2));
		}
		while ((readLine = br.readLine()) != null && readLine.length()>3) {
			System.out.println(readLine);
			Pattern p = Pattern.compile("(\\S+):(.+)", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(readLine);
			if (m.find()) {
				String key = m.group(1);
				String value = m.group(2);
				header.addValue(key, value);
			}
		}
		if(readLine == null) {
			throw new NullPointerException("Klient rozłączył się");
		}
		return header;
	}

	/**
	 * Fabryka nagłówków odpowiedzi
	 * @author akozlowski
	 *
	 */
	static class ResponseFactory{
		/**
		 * Ukryty konstruktor
		 */
		private ResponseFactory(){}
		
		/**
		 * Na podstawie parametrów buduje odpowiedź na nagłówek od klienta
		 * @param header nagłówek na który ma zostać skonstruowana odpowiedź
		 * @param session aktualna sesja
		 * @return Skonstruowany nagłówek {@link Response} lub null gdy wystąpi błąd
		 * podczas konstrukcji odpowiedzi.
		 */
		public static Response createResponse(Header header, Session sesion) {
			try {
				if (!header.checkRtsp()) {
					return new NotSupportedResponse(header, sesion);
				} else {
					switch (header.getType()) {
					case OPTIONS:
						return new OptionsResponse(header, sesion);
					case SETUP:
						return new SetupResponse(header, sesion);
					case DESCRIBE:
						return new DescribeResponse(header, sesion);
					case PLAY:
						return new PlayResponse(header, sesion);
					case TEARDOWN:
						return new TeardownResponse(header, sesion);
					case GET_PARAMETER:
						return new GetParameterResponse(header, sesion);
					default:
						return new OptionsResponse(header, sesion);
					}
				}
			} catch (Exception e) {
				Log.w(Tags.RESPONSE, "Parsowanie odpowiedzi poszło nie tak: "+e.getMessage());
				return null;
			}
		}
	}

	@Override
	public void close() throws IOException {
		session.stopAll();
		this.br.close();
		this.bw.close();
		this.socket.close();
	}
}
