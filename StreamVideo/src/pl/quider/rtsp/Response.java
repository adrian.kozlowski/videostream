package pl.quider.rtsp;

import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.quider.StreamVideo.stream.Session;
import pl.quider.util.HeaderStates;

/**
 *  Przykładowy kod nagłówka:
 * <code><br>
 * RTSP/1.0 200 OK <br>
 * Server: Real <br>
 * CSeq: RTSPSeqNb <br>
 * Public: DESCRIBE,SETUP,TEARDOWN,PLAY,PAUSE,GET_PARAMETER <br>
 * //jedna wolna linia
 * </code>
 * @author Adrian
 * 
 */
public abstract class Response {
	protected final String CRLF = "\r\n";
	
	/**
	 * Nagłówek klienta
	 */
	protected Header header;
	
	/**
	 * Budowany nagłówek w odpowiedzi
	 */
	private StringBuilder responseHeader = new StringBuilder();
	
	/**
	 * Aktualna sekwencja
	 */
	protected String CSeq;
	
	/**
	 * Flaga sprawdzająca poprawne sparsowanie
	 */
	protected boolean correctParsed = true;
	
	/**
	 * Port nadawania
	 */
	protected int localPort;
	
	/**
	 * Zawartość przesyłanego nagłówka - najczęściej będzie używany w DESCRIBE
	 * do wysyłania SDP
	 */
	protected String content;
	
	/**
	 * Ciało odpowiedzi
	 */
	protected String propeties;
	
	/**
	 * Status odpowiedzi powinien być przypisany z 
	 * {@link HeaderStates}. Domyślnie OK
	 */
	protected String status = HeaderStates.OK;
	
	/**
	 * Sesja 
	 */
	protected Session session;

	/**
	 * Ukryty konstruktor bezparametrowy
	 */
	@SuppressWarnings("unused")
	private Response() {
	}

	public Response(Header header, Session session) {
		this.header = header;
		this.session = session;
		this.content = new String();
		composeResponse();
	}

	/**
	 * Metoda komponująca odpowiedź na konkretny nagłówek
	 */
	protected abstract void composeResponse();

	@Override
	public String toString() {
		responseHeader = new StringBuilder();
		try {
			String fieldValues = header.getFieldValues("CSeq");
			Pattern p;
			Matcher m;
			p = Pattern.compile("(\\d)");
			m = p.matcher(fieldValues);
			if (m.find()) {
				String group = m.group();
				this.CSeq = "CSeq: " + group;
			} else {
				throw new Exception("Brak sekwencji");
			}

		} catch (Exception e) {
			this.CSeq = "CSeq: -1";
		}

		responseHeader.append("RTSP/1.0 ");
		responseHeader.append(this.status);
		responseHeader.append(CRLF);
		responseHeader.append("Server: Adrian real RTSP Server");
		responseHeader.append(CRLF);
		responseHeader.append(this.CSeq);
		responseHeader.append(CRLF);
		responseHeader.append("Content-Length: " + this.content.length());
		responseHeader.append(CRLF);
		responseHeader.append(this.propeties);
		responseHeader.append(CRLF);

		responseHeader.append(content);

		return responseHeader.toString();
	}

	/**
	 * Metoda sprawdzającam, czy parsowanie przebiegło poprawnie
	 * @return prawda jeśli sparsowano. W innym przypadku fałsz.
	 */
	public boolean isCorrectParsed() {
		return correctParsed;
	}

	/**
	 * Setter flagi potwierdzającej poprawne sparsowanie odpowiedzi
	 * @param correctParsed
	 */
	public void setCorrectParsed(boolean correctParsed) {
		this.correctParsed = correctParsed;
	}

	/**
	 * Wysyła odpowiedź do serwera
	 * @param output
	 * @throws IOException
	 */
	public void send(OutputStream output) throws IOException{
		output.write(this.toString().getBytes());
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
