package pl.quider.rtsp.response;

import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;
import pl.quider.util.HeaderStates;

public class GetParameterResponse extends Response {

	public GetParameterResponse(Header header, Session  session) {
		super(header, session);
	}

	@Override
	protected void composeResponse() {
		status = HeaderStates.OK;
		propeties=CRLF;
	}

}
