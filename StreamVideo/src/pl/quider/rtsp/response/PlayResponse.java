package pl.quider.rtsp.response;

import java.net.InetAddress;

import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;
import pl.quider.util.HeaderStates;

public class PlayResponse extends Response {

	public PlayResponse(Header header, Session session) {
		super(header, session);
	}

	@Override
	protected void composeResponse() {
		InetAddress client = session.getOrigin();
		
		String playPropeties = "RTP-Info: ";
		if (session.trackExists(0)){
			playPropeties += "url=rtsp://"+client.getHostAddress()+":"+session.getTrackLocalPort(0)+"/trackID="+0+";seq=0,";
		}
		if (session.trackExists(1)){
			playPropeties += "url=rtsp://"+client.getHostAddress()+":"+session.getTrackLocalPort(1)+"/trackID="+1+";seq=0,";
		}
		playPropeties = playPropeties.substring(0, playPropeties.length()-1) + "\r\nSession: 1185d20035702ca\r\n";
		
		this.propeties = playPropeties;
		this.status = HeaderStates.OK;
	}

}
