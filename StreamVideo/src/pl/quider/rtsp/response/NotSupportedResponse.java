package pl.quider.rtsp.response;

import java.net.InetAddress;

import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;
import pl.quider.util.HeaderStates;

public class NotSupportedResponse extends Response {

	public NotSupportedResponse(Header header, Session session) {
		super(header, session);
	}

	@Override
	protected void composeResponse() {
		status=HeaderStates.BAD_REQUEST;
		propeties = CRLF;
		
	}

}
