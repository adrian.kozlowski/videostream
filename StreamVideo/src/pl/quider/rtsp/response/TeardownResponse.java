package pl.quider.rtsp.response;

import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;

public class TeardownResponse extends Response {

	public TeardownResponse(Header header, Session session) {
		super(header, session);
	}

	@Override
	protected void composeResponse() {
		propeties = CRLF;
	}

}
