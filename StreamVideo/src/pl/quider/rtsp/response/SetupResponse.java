package pl.quider.rtsp.response;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

import pl.quider.StreamVideo.properties.Tags;
import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;
import pl.quider.util.HeaderStates;

public class SetupResponse extends Response {

	public SetupResponse(Header header, Session session) {
		super(header, session);
		this.header = header;
	}

	@Override
	protected void composeResponse() {
		String uri = this.header.getUri();
		// wyciągniemy z uri numer ścieżki
		Pattern p;
		Matcher m;
		p = Pattern.compile("trackID=(\\w+)", Pattern.CASE_INSENSITIVE);
		m = p.matcher(uri);

		if (!m.find()) { // jeżeli nie wiemy jaka to ścieżka to nie możemy wysysłać strumienia
			this.status = HeaderStates.BAD_REQUEST;
			return;
		}
		int trackId = Integer.parseInt(m.group(1));

		if (!session.trackExists(trackId)) { // poprawnie sformuowany uri ale nie można znaleźć takiej ścieżki
			status = HeaderStates.NOT_FOUND;
			return;
		}
		
		String transportLine = header.getFieldValues("Transport");
		p = Pattern.compile("client_port=(\\d+)-(\\d+)",Pattern.CASE_INSENSITIVE);
		m = p.matcher(transportLine);
		
		int clientPort1;
		int clientPort2;
		if (!m.find()) {
			int port = session.getTrackDestinationPort(trackId);
			clientPort1 = port;
			clientPort2 = port+1;
		}
		else {
			clientPort1 = Integer.parseInt(m.group(1));
			clientPort2 = Integer.parseInt(m.group(2));
		}

		int ssrc = session.getTrackSSRC(trackId);
		int src = session.getTrackLocalPort(trackId);
		session.setTrackDestinationPort(trackId, clientPort1);

		StringBuilder setupResponsePropeties = new StringBuilder();

		setupResponsePropeties.append("Transport: RTP/AVP/UDP;");
		setupResponsePropeties.append(session.getRoutingScheme());
		setupResponsePropeties.append(";destination=").append(session.getDestination().getHostAddress());
		setupResponsePropeties.append(";client_port=").append(clientPort1).append("-").append(clientPort2);
		setupResponsePropeties.append(";server_port=").append(src).append("-").append(src + 1);
		setupResponsePropeties.append(";ssrc=").append(Integer.toHexString(ssrc));
		setupResponsePropeties.append(";mode=play");
		setupResponsePropeties.append(CRLF);
		setupResponsePropeties.append("Session: 1185d20035702ca");
		setupResponsePropeties.append(CRLF);
		setupResponsePropeties.append("Cache-Control: no-cache");
		setupResponsePropeties.append(CRLF);
		
		//jeszcze musimy odesłać nagłówek
		status = HeaderStates.OK;
		propeties = setupResponsePropeties.toString();
		try {
			session.start(trackId);
		} catch (IllegalStateException e) {
			Log.e(Tags.SESSION, "Nie udało się wystartować strumienia z sesji");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(Tags.SESSION, "Nie udało się wystartować strumienia z sesji");
		}
	}

}
