package pl.quider.rtsp.response;

import java.io.IOException;

import pl.quider.StreamVideo.R;
import pl.quider.StreamVideo.StreamVideoActivity;
import pl.quider.StreamVideo.properties.Tags;
import pl.quider.StreamVideo.stream.Session;
import pl.quider.rtsp.Header;
import pl.quider.rtsp.Response;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class DescribeResponse extends Response {

	public DescribeResponse(Header header, Session ses) {
		super(header,ses);
	}
	
	/**
	 * 
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	private void parseHeaderUri() throws IllegalStateException, IOException{
		if (session.getTrackCount()==0){
			session.addVideoTrack(flashHandle());
		}
	}
	
	/**
	 * Sprawdzenie we właściwościach czy uruchomiona jest lampa
	 * i ewentualne jej włączenie.
	 * @return
	 */
	private boolean flashHandle() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(StreamVideoActivity.context);
		boolean led = preferences.getBoolean(StreamVideoActivity.context.getString(R.string.prop_key_led), false);
		return led;
	}
	
	@Override
	protected void composeResponse() {
		try {
			parseHeaderUri();
		} catch (IllegalStateException e) {
			Log.e(Tags.DESCRIBE_RESPONSE, e.getMessage());
		} catch (IOException e) {
			Log.e(Tags.DESCRIBE_RESPONSE, e.getMessage());
		}
		
		String sessionDescription = null;
		try {
			sessionDescription = session.getSessionDescription();
		} catch (IllegalStateException e) {
			Log.e(Tags.DESCRIBE_RESPONSE, "Błąd: "+e.getMessage());
		} catch (IOException e) {
			Log.e(Tags.DESCRIBE_RESPONSE, "Błąd: "+e.getMessage());
		}
		
		content = sessionDescription; //zawartość
		
		StringBuilder sb = new StringBuilder();
		sb.append("Content-Base: "+session.getDestination().getHostAddress()+":"+session.getDestinationPort()+"/");
		sb.append(CRLF);
		sb.append("Content-Type: application/sdp");
		sb.append(CRLF);
		sb.append("Content-Length: ");
		sb.append(content.length());
		sb.append(CRLF);
		propeties = sb.toString();
	}

}
