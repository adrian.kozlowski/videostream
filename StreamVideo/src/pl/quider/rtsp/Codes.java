/**
 * 
 */
package pl.quider.rtsp;

/**
 * @author Adrian
 *
 */
public class Codes {
	public static int CONTINUE = 100;
	public static int OK = 200; 
	public static int Created = 201; 
	public static int Low_on_Storage_Space = 250;
	public static int Multiple_Choices = 300; 
	public static int Moved_Permanently = 301; 
	public static int Moved_Temporarily = 302; 
	public static int See_Other = 303; 
	public static int Not_Modified = 304; 
	public static int Use_Proxy = 305; 
	public static int Bad_Request = 400; 
	public static int Unauthorized = 401;
	public static int Payment_Required = 402; 
	public static int Forbidden = 403; 
	public static int Not_Found = 404; 
	public static int Method_Not_Allowed = 405; 
	public static int Not_Acceptable  = 406; 
	public static int Proxy_Authentication_Required = 407; 
	public static int Request_Timeout = 408;
	public static int Gone = 410; 
	public static int Length_Required = 411; 
	public static int Precondition_Failed = 412; 
	public static int Request_Entity_Too_Large = 413; 
	public static int RequestURI_Too_Large = 414; 
	public static int Unsupported_Media_Type = 415;
	public static int Parameter_Not_Understood = 451; 
	public static int Conference_Not_Found = 452; 
	public static int Not_Enough_Bandwidth = 453; 
	public static int Session_Not_Found = 454; 
	public static int Method_Not_Valid_in_This_State = 455; 
	public static int Header_Field_Not_Valid_for_Resource = 456;
	public static int Invalid_Range = 457;
	public static int Parameter_Is_ReadOnly = 458; 
	public static int Aggregate_operation_not_allowed = 459; 
	public static int Only_aggregate_operation_allowed = 460;
	public static int Unsupported_transport = 461; 
	public static int Destination_unreachable = 462; 
    public static int Internal_Server_Error = 500; 
    public static int Not_Implemented = 501; 
    public static int Bad_Gateway = 502; 
    public static int Service_Unavailable = 503; 
    public static int Gateway_Timeout = 504; 
    public static int RTSP_Version_not_supported = 505; 
    public static int Option_not_supported = 551; 
}
