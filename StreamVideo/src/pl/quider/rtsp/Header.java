/**
 * 
 */
package pl.quider.rtsp;

import java.util.HashMap;

import pl.quider.util.HeaderType;

/**
 * @author Adrian
 * 
 */
public class Header {
	private String uri;
	private HeaderType type;
	private boolean correctParsed = true;



	private String rtsp = "RTSP/1.0 ";
	private HashMap<String, String> propeties;

	/**
	 * 
	 * @param type
	 * @param url
	 */
	public Header(HeaderType type, String url) {
		uri = url;
		this.type = type;
	}

	/**
	 * 
	 */
	public Header() {
		propeties = new HashMap<String, String>();
	}

	/**
	 * 
	 * @param key
	 */
	public void addField(String key) {
		propeties.put(key, new String());
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getFieldValues(String key){
		if(propeties.containsKey(key)){
			return propeties.get(key);
		}
		return new String();
	}

	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addValue(String key, String value) {
		if (propeties.containsKey(key)) {
			String property = propeties.get(key);
			property += ";"+value;
			propeties.put(key, property);
		} else{
			propeties.put(key, value);
		}
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(HeaderType type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public HeaderType getType(){
		return this.type;
	}
	/**
	 * 
	 * @return
	 */
	public String getRtsp() {
		return rtsp;
	}
	
	/**
	 * 
	 * @param rtsp
	 */
	public void setRtsp(String rtsp){
		this.rtsp = rtsp;
	}

	/**
	 * 
	 * @param rtsp
	 */
	public boolean checkRtsp() {
		return "RTSP/1.0".equals(this.rtsp.trim());
	}
	
	/**
	 * 
	 * @return
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * 
	 * @param uri
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	public boolean isCorrectParsed() {
		return correctParsed;
	}

	public void setCorrectParsed(boolean correctParsed) {
		this.correctParsed = correctParsed;
	}

	@Override
	public String toString() {
//		StringBuilder sb = new StringBuilder();
		return super.toString();
	}
	
}
