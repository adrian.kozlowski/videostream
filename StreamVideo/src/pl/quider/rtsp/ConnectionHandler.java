/**
 * 
 */
package pl.quider.rtsp;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

import pl.quider.StreamVideo.properties.Tags;
import android.util.Log;

/**
 * Insancja klasy zostaje sworzona kiedy {@link RTSPServer#start} zostanie
 * wywołana
 * 
 * @author Adrian
 * 
 */
public class ConnectionHandler extends Thread implements Runnable, Closeable {
	private int port;
	
	/**
	 * Serwer TCP/IP do nasluchiwania nagłówków
	 */
	private ServerSocket server;
	
	/**
	 * Przekazany listener zdarzeń sesji
	 */
	private StreamSessionListener streamVideoActivity;
	
	/**
	 * Zbiór uruchomionych klientów
	 */
	private Set<ClientThread> clients = new HashSet<ClientThread>();

	/**
	 * 
	 * @param port
	 * @param streamVideoActivity 
	 */
	public ConnectionHandler(int port, StreamSessionListener streamVideoActivity) {
		try {
			this.port = port;
			this.streamVideoActivity = streamVideoActivity;
			server = new ServerSocket(this.port);
			Log.i(Tags.CONNECTION_MANAGER, "Serwer RTSP nasuchuje na porcie" + this.port);

		} catch (IOException e) {
			Log.e(Tags.CONNECTION_MANAGER, "Nie udało si otworzyć socketa servera: " + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {
				new ClientThread(server.accept(), this.streamVideoActivity).start();
			} catch (IOException e) {
				Log.e(Tags.CONNECTION_MANAGER, "Nie udało si nawiązać połączenia z klientem: " + e.getMessage());
			}
		}
	}
	/**
	 * 
	 * @return
	 */
	public ServerSocket getServer() {
		return this.server;
	}

	@Override
	public void close() throws IOException {
		getServer().close();
		for (ClientThread client : clients) {
			client.close();
		}
		System.gc();
	}
}
