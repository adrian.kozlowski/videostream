/**
 * 
 */
package pl.quider.rtsp;

import java.io.IOException;

import pl.quider.StreamVideo.properties.Tags;
import android.util.Log;

/**
 * Instancja klasy reprezentuje serwer RTSP zdolny do włączenia i wyłączenia co
 * za tym idzie zaniechania wszystkich czynności
 * 
 * @author Adrian
 * 
 */
public class RTSPServer {

	/**
	 * Port lokalny
	 */
	private int port;
	
	/**
	 * Flaga rozpoczęcia
	 */
	private boolean started = false;
	
	/**
	 * Uchwyt połączenia
	 */
	private ConnectionHandler connectionHandler;
	
	/**
	 * Przekazany listener sesji
	 */
	private StreamSessionListener streamVideoActivity;

	/**
	 * @param streamVideoActivity 
	 * 
	 */
	public RTSPServer(int port, StreamSessionListener streamVideoActivity) {
		this.port = port;
		this.streamVideoActivity = streamVideoActivity;
	}

	/**
	 * Staruje serwer RTSP
	 */
	public synchronized void start() {
		if (!started) {
			started = true;
			connectionHandler = new ConnectionHandler(this.port, this.streamVideoActivity);
			connectionHandler.start();
		}
	}

	/**
	 * Zatrzymuje serwer RTSP
	 */
	public synchronized void stop() {
		try {
			connectionHandler.close();
			started = false;
		} catch (IOException e) {
			Log.e(Tags.RTSP, "Błąd podczas wyłączania serwera RTSP");
		}
	}

}
