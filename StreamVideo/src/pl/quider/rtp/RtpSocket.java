package pl.quider.rtp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Random;

/**
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |V=2|P|X|  CC   |M|     PT      |       sequence number         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                          timestamp                            |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |           synchronization source (SSRC) identifier            |
 * +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * |            contributing source (CSRC) identifiers             |
 * |                             ....                              |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * @throws IOException
 */
public class RtpSocket {

	private MulticastSocket usock;
	private DatagramPacket upack;
	
	private byte[] buffer = new byte[MTU];
	private int seq = 0;
	private boolean upts = false;
	private int ssrc;
	private int port = -1;
	
	public static final int RTP_HEADER_LENGTH = 12;
	public static final int MTU = 1500;
	
	
	public RtpSocket() throws IOException {
		
		buffer[0] = (byte) Integer.parseInt("10000000",2);
		
		/**
		 * RFC 3551
		 */
		buffer[1] = (byte) 96;
		
		setLong((ssrc=(new Random()).nextInt()),8,12);
		
		usock = new MulticastSocket();
		upack = new DatagramPacket(buffer, 1);

	}

	public void close() {
		usock.close();
	}
	
	public void setSSRC(int ssrc) {
		this.ssrc = ssrc; 
		setLong(ssrc,8,12);
	}
	
	public int getSSRC() {
		return ssrc;
	}
	
	public void setTimeToLive(int ttl) throws IOException {
		usock.setTimeToLive(ttl);
	}
	
	public void setDestination(InetAddress dest, int dport) {
		port = dport;
		upack.setPort(dport);
		upack.setAddress(dest);
	}
	
	/**
	 * Zwraca bufor pakietu, który można edytować w całości przed wysłaniem.
	 * @return byte[] tablica bufora
	 */
	public byte[] getBuffer() {
		return buffer;
	}
	
	/**
	 * 
	 * @return int port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * 
	 * @return int lokalny port
	 */
	public int getLocalPort() {
		return usock.getLocalPort();
	}
	
	/**
	 * Wysyła ramkę RTP. 
	 * @param length
	 * @throws IOException
	 */
	public void send(int length) throws IOException {
		updateSequence();
		upack.setLength(length);
		usock.send(upack);
		if (upts) {
			upts = false;
			buffer[1] -= 0x80;
		}
		
	}
	
	/**
	 * Inkrementuje numer sekwencji
	 */
	private void updateSequence() {
		setLong(++seq, 2, 4);
	}
	
	/** 
	 * Nadpisuje timestamp
	 * @param timestamp nowy timestamp
	 **/
	public void updateTimestamp(long timestamp) {
		setLong(timestamp, 4, 8);
	}
	
	/**
	 * 
	 */
	public void markNextPacket() {
		upts = true;
		buffer[1] += 0x80; // następny pakiet
	}
	
	/**
	 * Ustawia liczbę na bajtach
	 * @param n liczba
	 * @param begin początkowy bit
	 * @param end końcowy bit
	 */
	private void setLong(long n, int begin, int end) {
		for (end--; end >= begin; end--) {
			buffer[end] = (byte) (n % 256);
			n >>= 8;
		}
	}	
	
}
