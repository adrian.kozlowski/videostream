package pl.quider.rtp;

import java.io.IOException;

import pl.quider.StreamVideo.properties.Tags;

import android.os.SystemClock;
import android.util.Log;

/**
 *   RFC 4629.
 * 
 *   H.263 Streaming 
 *   
 */
public class H263Packetizer extends AbstractPacketizer implements Runnable {

	private final static int MAXPACKETSIZE = 1400;
	private Statistics stats = new Statistics();
	
	private Thread t;
	
	public H263Packetizer() throws IOException {
		super();
	}
	
	public void start() throws IOException {
		if (!running) {
			running = true;
			t = new Thread(this);
			t.start();
		}
	}

	public void stop() {
		try {
			is.close();
		} catch (IOException ignore) {}
		running = false;
		try {
			t.join();
		} catch (InterruptedException e) {}
	}

	public void run() {
		long time, duration = 0, ts = 0;
		int i = 0, j = 0, tr;
		boolean firstFragment = true;
		
		try {
			skipHeader();
		} catch (IOException e) {
			Log.e(Tags.PACKETIZER,"Nie udało się przeskoczyć nagłókwka mp4");
			return;
		}	
		
		// Każdy pakiet, który przesyłamy posiada dwubajtowy nagłówek (RFC 4629, 5.1)
		buffer[rtphl] = 0;
		buffer[rtphl+1] = 0;
		
		try { 
			while (running) {
				time = SystemClock.elapsedRealtime();
				if (fill(rtphl+j+2,MAXPACKETSIZE-rtphl-j-2)<0) return;
				duration += SystemClock.elapsedRealtime() - time;
				j = 0;
//				Każda ramka w h263 zaczyna się 0000 0000 0000 0000 1000 00xx
//				stąd szukamy początku następnej ramki
				for (i=rtphl+2;i<MAXPACKETSIZE-1;i++) {
					if (buffer[i]==0 && buffer[i+1]==0 && (buffer[i+2]&0xFC)==0x80) {
						j=i;
						break;
					}
				}
				tr = (buffer[i+2]&0x03)<<6 | (buffer[i+3]&0xFF)>>2;
				Log.d(Tags.PACKETIZER,"j: "+j+" buf: "+printBuffer(buffer, rtphl, rtphl+5)+" tr: "+tr);
				if (firstFragment) {
//					początek ramki -> ustawiany nagłówek na 0x0400
					buffer[rtphl] = 4;
					firstFragment = false;
				} else {
					buffer[rtphl] = 0;
				}
				if (j>0) {
//					Gdy znajdzie się koniec ramki
					stats.push(duration);
					ts+= stats.average(); duration = 0;
					Log.d(Tags.PACKETIZER,"Koniec ramki! czas trwania: "+stats.average());
//					Zaznaczam ostatni element ramki 
					socket.markNextPacket();
					socket.send(j);
					socket.updateTimestamp(ts*90);
					System.arraycopy(buffer,j+2,buffer,rtphl+2,MAXPACKETSIZE-j-2); 
					j = MAXPACKETSIZE-j-2;
					firstFragment = true;
				} else {
//					W przypadku, gdy nie znajdziemy kolejenej ramki, uznajemy, pakiet zawiera całą ramkę.
					socket.send(MAXPACKETSIZE);
				}
			}
		} catch (IOException e) {
			running = false;
			Log.e(Tags.PACKETIZER,"IOException: "+e.getMessage());
			e.printStackTrace();
		}
		
		Log.d(Tags.PACKETIZER,"Pakietyzer za zatrzymany!");
			
	}

	/**
	 * 
	 * @param offset
	 * @param length
	 * @return
	 * @throws IOException
	 */
	private int fill(int offset,int length) throws IOException {
		
		int sum = 0, len;
		
		while (sum<length) {
			len = is.read(buffer, offset+sum, length-sum);
			if (len<0) {
				throw new IOException("EOS");
			}
			else sum+=len;
		}
		
		return sum;
			
	}
	
	/**
	 * Klient może odesłać nagłówki, których nie potrzeba. Przeskok nagłówków.
	 * @throws IOException
	 */
	private void skipHeader() throws IOException {
		while (true) {
			while (is.read() != 'm');
			is.read(buffer,rtphl,3);
			if (buffer[rtphl] == 'd' && buffer[rtphl+1] == 'a' && buffer[rtphl+2] == 't') break;
		}
	}
	
	/**
	 * 
	 * @author Adrian
	 *
	 */
	private static class Statistics {
		
		public final static int COUNT=50;
		private float m = 0, q = 0;
		
		public void push(long duration) {
			m = (m*q+duration)/(q+1);
			if (q<COUNT) q++;
		}

		public long average() {
			return (long)m;
		}

	}
	
}
