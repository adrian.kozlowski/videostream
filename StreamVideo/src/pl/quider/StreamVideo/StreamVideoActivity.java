package pl.quider.StreamVideo;

import java.io.IOException;

import pl.quider.StreamVideo.activities.PreferencesActivity;
import pl.quider.StreamVideo.api.Stream;
import pl.quider.StreamVideo.api.VideoStream;
import pl.quider.StreamVideo.properties.Tags;
import pl.quider.StreamVideo.stream.LiveVideo;
import pl.quider.StreamVideo.stream.Session;
import pl.quider.StreamVideo.stream.VideoQuality;
import pl.quider.rtsp.RTSPServer;
import pl.quider.rtsp.StreamSessionListener;
import pl.quider.util.MediaRecordStates;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

/**
 * {@link Activity} - główne okienko programu
 * 
 * @author adrian
 * 
 */
public class StreamVideoActivity extends Activity implements StreamSessionListener {

	/**
	 * Status aplikacji dot {@link MediaRecorder}
	 */
	private MediaRecordStates state;
	
	/**
	 * Powierzchnia przeznaczona do pokazania podglądu obrazu
	 */
	private SurfaceView surface;
	
	/**
	 * uchwyt 
	 */
	private SurfaceHolder holder;

	/**
	 * Serwer RTSP
	 */
	private RTSPServer RtspServer;
	
	/**
	 * port RTSP
	 */
	private int port = 8086;
	
	/**
	 * kontekst aplikacji
	 */
	public static Context context;
	
	/**
	 * Przeciążona metoda z nadklasy {@link Activity} wywoływana przez system
	 * Android przy uruchamianiu aplikacji
	 * 
	 * @Override
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = this;
		initPropeties();
		state = MediaRecordStates.STANDBY;
		//ustawiam główny layout
		setContentView(R.layout.main);
		surface = (SurfaceView) findViewById(R.id.surface); //znajduję surface i wrzucam do pola
		surface.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		holder = surface.getHolder();
	}
	
	/**
	 * Inicjalizacja właściwości (prawdopodobnie tylko pierwsze uruchomienie
	 * programu ever)
	 */
	private void initPropeties() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		this.port = sharedPreferences.getInt(StreamVideoActivity.context.getString(R.string.prop_key_port), 8086);
	}

	/**
	 * obsługa klikniętych menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.propeties: {
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivity(intent);
			return true;
		}
		case R.id.serverStopStart: {
			try {
				if (state.equals(MediaRecordStates.STANDBY)) {
					onServerStart();
					return true;
				} else {
					stopRecording();
					return true;
				}
			} catch (Exception e) {
				Log.e(Tags.SERVERS_ENABL, "Błąd podczas uruchamiania Mediarecorder " + e.getMessage());
			}
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Zatrzymanie serwera RTSP
	 * 
	 * @throws IOException
	 */
	private void stopRecording() throws IOException {
		this.RtspServer.stop();
		surface = null;
		Log.d(Tags.RTSP, "Serwer RTSP zastopowany");
	}

	/**
	 * Uruchamia lokalny i zewnętrzny serwer oraz oczekuje połączenia z lokalnym
	 * socketem
	 */
	private void onServerStart() {
		this.RtspServer = new RTSPServer(this.port, this);
		RtspServer.start();
		Session.setSurfaceHolder(holder, true);
	}

	/**
	 * 
	 */
	@Override
	protected void onPause() {
		super.onPause();
	}

	/**
	 * 
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	/**
	 * 
	 */
	@Override
	protected void onResume() {
		super.onResume();
	}

	/**
	 * Otwiera menu główne po nacisnieciu na przycisk opcji
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}

	/**
	 * @return the state
	 */
	public MediaRecordStates getState() {
		return state;
	}

	/**
	 * @param state Ustawia stan aplikacji
	 */
	public void setState(MediaRecordStates state) {
		this.state = state;
	}

	@Override
	public void onStart(Session session) {
		setState(MediaRecordStates.RECORDING);
		
	}

	@Override
	public void onStop(Session session) {
		setState(MediaRecordStates.STANDBY);
		
	}

}