package pl.quider.StreamVideo.api;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Interface reprezentujący strumień 
 */
public interface Stream {

	public void start() throws IllegalStateException;
	public void prepare() throws IllegalStateException,IOException;
	public void stop();
	public void release();
	
	public void setTimeToLive(int ttl) throws IOException;
	public void setDestination(InetAddress dest, int dport);
	
	public int getLocalPort();
	public int getDestinationPort();
	public int getSSRC();
	
	public String generateSessionDescription() throws IllegalStateException, IOException;
	
	public boolean isStreaming();
	
}
