package pl.quider.StreamVideo.api;

import java.io.IOException;
import java.net.InetAddress;

import pl.quider.rtp.AbstractPacketizer;
import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;

/**
 *  
 */
public abstract class MediaStream extends MediaRecorder implements Stream {

	public static final int MODE_STREAMING = 0;
	public static final int MODE_DEFAULT = 1;
	
	private static int id = 0;
	private int socketId;
	private LocalServerSocket lss = null;
	private LocalSocket receiver, sender = null;
	protected AbstractPacketizer packetizer = null;
	protected boolean streaming = false, modeDefaultWasUsed = false;
	protected String sdpDescriptor;
	protected int mode = MODE_STREAMING;
	
	/**
	 * Tworzy wewnętrzny serwer nasłuchujący połaczeń wewnętrznego klienta - 
	 * kodu odbierającego bajty z kamery 
	 * @throws IOException 
	 */
	public MediaStream() throws IOException {
		super();
		
		try {
			lss = new LocalServerSocket("StreamServer"+id);
		} catch (IOException e1) {
			throw new IOException("Nie mona utworzy lokalnego gniazda");
		}
		socketId = id;
		id++;
		
	}

	/**
	 * Strumien zostanie wysany na ten adres
	 * @param dest
	 * @param dport
	 */
	public void setDestination(InetAddress dest, int dport) {
		this.packetizer.setDestination(dest, dport);
	}
	
	/** 
	 * Set the Time To Live of the underlying RtpSocket 
	 * @throws IOException 
	 */
	public void setTimeToLive(int ttl) throws IOException {
		this.packetizer.setTimeToLive(ttl);
	}
	
	/**
	 * 
	 */
	public int getDestinationPort() {
		return this.packetizer.getRtpSocket().getPort();
	}
	
	/**
	 * 
	 */
	public int getLocalPort() {
		return this.packetizer.getRtpSocket().getLocalPort();
	}
	
	/**
	 * 
	 * @param mode
	 * @throws IllegalStateException
	 */
	public void setMode(int mode) throws IllegalStateException {
		if (!streaming) {
			this.mode = mode;
			if (mode == MODE_DEFAULT) modeDefaultWasUsed = true;
		}
		else {
			throw new IllegalStateException("You can't call setMode() while streaming !");
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public AbstractPacketizer getPacketizer() { 
		return packetizer;
	}
	
	/**
	 * 
	 */
	public boolean isStreaming() {
		return streaming;
	}
	
	/**
	 * 
	 */
	public void prepare() throws IllegalStateException,IOException {
		if (mode==MODE_STREAMING) {
			createSockets();
			// We write the ouput of the camera in a local socket instead of a file !			
			// This one little trick makes streaming feasible quiet simply: data from the camera
			// can then be manipulated at the other end of the socket
			setOutputFile(sender.getFileDescriptor());
		} else {
			
		}
		super.prepare();
	}
	/**
	 * Startuje strumień
	 */
	public void start() throws IllegalStateException {
		super.start();
		try {
			if (mode==MODE_STREAMING) {
				// receiver.getInputStream contains the data from the camera
				// the packetizer encapsulates this stream in an RTP stream and send it over the network
				packetizer.setInputStream(receiver.getInputStream());
				packetizer.start();
			}
			streaming = true;
		} catch (IOException e) {
			super.stop();
			throw new IllegalStateException("Something happened with the local sockets :/ Start failed");
		} catch (NullPointerException e) {
			super.stop();
			throw new IllegalStateException("setPacketizer() should be called before start(). Start failed");
		}
	}
	
	/**
	 * Zatrzymuje strumień
	 */
	public void stop() {
		if (streaming) {
			try {
				super.stop();
			}
			catch (Exception ignore) {}
			finally {
				super.reset();
				closeSockets();
				if (mode==MODE_STREAMING) packetizer.stop();
				streaming = false;
			}
		}
	}
	
	/**
	 * 
	 */
	public int getSSRC() {
		return getPacketizer().getRtpSocket().getSSRC();
	}
	
	/**
	 * Generuje plik opisu sesji zgodny z protokołem SDP wysyłanym w zawartości nagłówka
	 * DESCRIPTION
	 */
	public abstract String generateSessionDescription()  throws IllegalStateException, IOException;
	
	/**
	 * Tworzy lokalne gniazda na zasadzie server - client, 
	 * gdzie serwer kamery nasłuchuje połączeń klienta czyli kodu 
	 * odbierającego bajty z kamery
	 * @throws IOException
	 */
	private void createSockets() throws IOException {
		receiver = new LocalSocket();
		receiver.connect( new LocalSocketAddress("StreamServer" + socketId ) );
		receiver.setReceiveBufferSize(500000);
		sender = lss.accept();
		sender.setSendBufferSize(500000); 
	}
	
	/**
	 * zamyka lokalne gniazda
	 */
	private void closeSockets() {
		try {
			sender.close();
			receiver.close();
		} catch (Exception ignore) {}
	}
	
	/**
	 * zwalnia zasoby
	 */
	public void release() {
		stop();
		try {
			lss.close();
		}
		catch (Exception ignore) {}
		super.release();
	}
	
}
