package pl.quider.StreamVideo.api;

import java.io.IOException;

import pl.quider.StreamVideo.StreamVideoActivity;
import pl.quider.StreamVideo.properties.Tags;
import pl.quider.StreamVideo.stream.VideoQuality;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;

/**
 * 
 * @author akozlowski
 *
 */
public abstract class VideoStream extends MediaStream {

	protected VideoQuality quality = VideoQuality.defaultVideoQualiy.clone();
	protected SurfaceHolder.Callback surfaceHolderCallback = null;
	protected Surface surface = null;
	protected boolean flashState = false;  
	protected boolean qualityHasChanged = false;
	protected int videoEncoder, cameraId = 0;
	protected Camera camera;

	/** 
	 * Nie używać klasy bezpośrednio!
	 * @param cameraId może być CameraInfo.CAMERA_FACING_BACK lub CameraInfo.CAMERA_FACING_FRONT
	 * @throws IOException 
	 */
	public VideoStream(int camera) throws IOException {
		super();
		CameraInfo cameraInfo = new CameraInfo();
		int numberOfCameras = Camera.getNumberOfCameras();
		for (int i=0;i<numberOfCameras;i++) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == camera) {
				this.cameraId = i;
				break;
			}
		}
	}
	
	/**
	 * Ustawia powierzchnię do pokazania podglądu filmowanej przestrzeni.
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 */
	public void setPreviewDisplay(Surface surface) {
		this.surface = surface;
	}
	
	/**
	 * Właczenie lub wyłączenie lampy błyskowej
	 * @param state
	 */
	public void setFlashState(boolean state) {
		flashState = state;
	}
	
	/** 
	 * Ustawia rozdzielczość strumienia
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 * @param width szerokość strumienia
	 * @param height wysokość strumienia
	 */
	public void setVideoSize(int width, int height) {
		if (quality.resX != width || quality.resY != height) {
			quality.resX = width;
			quality.resY = height;
			qualityHasChanged = true;
		}
	}
	
	/** 
	 * Ustawia framerate strumienia
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 * @param rate Framerate strumienia
	 */	
	public void setVideoFramerate(int rate) {
		if (quality.framerate != rate) {
			quality.framerate = rate;
			qualityHasChanged = true;
		}
	}
	
	/** 
	 * Modyfikuje bitrate strumienia
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 * @param bitrate Bitrate strumienia w b/s
	 */	
	public void setVideoEncodingBitrate(int bitrate) {
		if (quality.bitrate != bitrate) {
			quality.bitrate = bitrate;
			qualityHasChanged = true;
		}
	}
	
	/** 
	 * Modyfikuje jakość strumienia
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 * @param videoQuality Jakość strumienia
	 */
	public void setVideoQuality(VideoQuality videoQuality) {
		if (!quality.equals(videoQuality)) {
			quality = videoQuality;
			qualityHasChanged = true;
		}
	}
	
	/** 
	 * Modyfikuje enkoder wideo strumienia.
	 * Mozna wywołać zawsze, ale ustawienia zmieniają się po wywołaniu prepare()
	 * @param videoEncoder Encoder strumienia
	 */
	public void setVideoEncoder(int videoEncoder) {
		this.videoEncoder = videoEncoder;
	}
	
	/*
	 * (non-Javadoc)
	 * @see pl.quider.StreamVideo.MediaStream#stop()
	 */
	@Override
	public synchronized void stop() {
		super.stop();
		if (camera != null) {
			try {
				camera.reconnect();
			} catch (Exception e) {
				Log.e(Tags.VIDEO_STREAM,e.getMessage()!=null?e.getMessage():"Błąd");
			}
			camera.stopPreview();
			try {
				camera.release();
			} catch (Exception e) {
				Log.e(Tags.VIDEO_STREAM,e.getMessage()!=null?e.getMessage():"Błąd");
			}
			camera = null;
		}
	}

	/**
	 * Przygotowuje strumień do metody {@link #start()}
	 * Kamera zostanie właczona i skonfigurowana
	 */
	@Override
	public void prepare() throws IllegalStateException, IOException, RuntimeException {
		if (camera == null) {
			try {
				camera = Camera.open(cameraId);
				camera.setErrorCallback(new Camera.ErrorCallback() {
					@Override
					public void onError(int error, Camera camera) {
						if (error == Camera.CAMERA_ERROR_SERVER_DIED) {
							Log.e(Tags.VIDEO_STREAM,"Krytyczny błąd serwera kamery");
							stop();
						}	
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
		
			// Ponowne połączenie z kamerą żeby zmienić status lampy
			Parameters parameters = camera.getParameters();
			if (flashState) {
				if (parameters.getFlashMode()==null) {
					throw new IllegalStateException("Nie można włączyć lampy!");
				} else {
					parameters.setFlashMode(flashState?Parameters.FLASH_MODE_TORCH:Parameters.FLASH_MODE_OFF);
					camera.setParameters(parameters);
				}
			}
			camera.setDisplayOrientation(quality.orientation);
			camera.unlock();
			super.setCamera(camera);

			//zastosowanie ewentualnych zmian
			super.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			super.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			if (mode==MODE_DEFAULT) {
				super.setMaxDuration(0);
				super.setMaxFileSize(Integer.MAX_VALUE);
			} else if (modeDefaultWasUsed) {
				try {
					super.setMaxDuration(0);
					super.setMaxFileSize(Integer.MAX_VALUE); 
				} catch (RuntimeException e) {
					Log.e(Tags.VIDEO_STREAM,"setMaxDuration or setMaxFileSize failed !");
				}
			}
			super.setVideoEncoder(videoEncoder);
			super.setPreviewDisplay(surface);
			super.setVideoSize(quality.resX,quality.resY);
			super.setVideoFrameRate(quality.framerate);
			super.setVideoEncodingBitRate(quality.bitrate);

			super.prepare();

			// reset ustawienia lampy do defaultowej wartosci w razie kolejnej zmiany
			flashState = false;

			qualityHasChanged = false;
		
		} catch (RuntimeException e) {
			camera.release();
			camera = null;
			throw e;
		} catch (IOException e) {
			camera.release();
			camera = null;
			throw e;
		}

	}
	
	/*
	 * (non-Javadoc)
	 * @see pl.quider.StreamVideo.MediaStream#generateSessionDescription()
	 */
	@Override
	public abstract String generateSessionDescription() throws IllegalStateException, IOException;

	/*
	 * (non-Javadoc)
	 * @see pl.quider.StreamVideo.MediaStream#release()
	 */
	@Override
	public void release() {
		stop();
		super.release();
	}
	
}
