/**
 * 
 */
package pl.quider.StreamVideo.activities;

import pl.quider.StreamVideo.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * @author akozlowski
 * 
 */
public class PreferencesActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
}
