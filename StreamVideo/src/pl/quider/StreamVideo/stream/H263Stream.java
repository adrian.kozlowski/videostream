package pl.quider.StreamVideo.stream;

import java.io.IOException;

import pl.quider.StreamVideo.api.VideoStream;
import pl.quider.rtp.H263Packetizer;
import android.media.MediaRecorder;

/**
 * Klasa strumieniowania H.263 z kamery poprzez RTP
 * setDestination() & setVideoSize() & setVideoFrameRate() & setVideoEncodingBitRate()
 * prepare() & start().
 */
public class H263Stream extends VideoStream {
	
	/**
	 * Ustawia encoder w Mediarecorderze na H263 i odpala paketizer h263
	 * @param cameraId
	 * @throws IOException
	 */
	public H263Stream(int cameraId) throws IOException {
		super(cameraId);
		setVideoEncoder(MediaRecorder.VideoEncoder.H263);
		this.packetizer = new H263Packetizer();
	}

	/**
	 * Zwraca opis strumienia jako cześć pliku (wysyłanego w zawartości nagłówka) SDP
	 */
	public String generateSessionDescription() throws IllegalStateException,
			IOException {

		return "m=video "+String.valueOf(getDestinationPort())+" RTP/AVP 96\r\n" +
				   "b=RR:0\r\n" +
				   "a=rtpmap:96 H263-1998/90000\r\n";
		
	}

}
