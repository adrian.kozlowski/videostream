/**
 * 
 */
package pl.quider.StreamVideo.stream;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

import pl.quider.StreamVideo.StreamVideoActivity;
import pl.quider.StreamVideo.api.Stream;
import pl.quider.StreamVideo.api.VideoStream;
import pl.quider.StreamVideo.properties.Tags;
import pl.quider.rtsp.StreamSessionListener;
import android.hardware.Camera.CameraInfo;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * @author Adrian
 * 
 */
public class Session {
	
	/**
	 * Sposób struminiowania tylko do jednego odbiorcy
	 * @value 1;
	 */
	private static final int UNICAST = 1;
	
	/**
	 * Dla formalności. Głównie będzie to UNICAST
	 * @value 2
	 */
	@SuppressWarnings("unused")
	private static final int MULTICAST = 2;

	/**
	 * Sposób określania jakości obrazu 
	 */
	private static VideoQuality defaultVideoQuality = VideoQuality.defaultVideoQualiy.clone();
	
	/**
	 *  Domyślnym encoderem jest i będzie H263 - dla uproszczenia
	 * private static int 
	 * defaultVideoEncoder = VIDEO_H263, 
	 * defaultAudioEncoder = AUDIO_AMRNB;
	 */
	private static int defaultCamera = CameraInfo.CAMERA_FACING_BACK;

	/**
	 * instancje sessji, które już strumieniują
	 */
	private static Session sessionUsingTheCamera = null;
	private static Session sessionUsingTheMic = null;

	/**
	 * ilość rozpoczętych strumieni
	 */
	private static int startedStreamCount = 0;

	private int sessionTrackCount = 0;

	/**
	 * Powierzchnia do namalowania podglądu z kamery
	 */
	private static SurfaceHolder surfaceHolder;
	
	/**
	 * Uchwyt powierzchni
	 */
	private static SurfaceHolder.Callback callback;
	
	/**
	 * Adres źródła (serwera) rtsp
	 */
	private InetAddress origin;
	
	/**
	 * Adres klienta rtsp
	 */
	private InetAddress destination;
	
	/**
	 * Rodzaj przesyłanych danych
	 */
	private int routingScheme = Session.UNICAST;
	
	/**
	 * TTL
	 */
	private int defaultTimeToLive = 64;
	
	/**
	 * Lista strumieni
	 */
	private Stream[] streamList = new Stream[2];
	
	/**
	 * Timestamp
	 */
	private long timestamp;
	
	/**
	 * Port klienta RTSP
	 */
	private int destinationPort;
	
	/**
	 * Zbiór listenerów zdarzeń
	 */
	private Set<StreamSessionListener> listeners = new HashSet<StreamSessionListener>();

	/**
	 * 
	 * @param origin
	 * @param destination
	 * @param destinationPort
	 */
	public Session(InetAddress origin, InetAddress destination, int destinationPort) {
		this.destination = destination;
		this.destinationPort = destinationPort;
		this.origin = origin;
		this.timestamp = System.currentTimeMillis();
	}

	/**
	 * 
	 * @param quality
	 */
	public static void setDefaultVideoQuality(VideoQuality quality) {
		defaultVideoQuality = quality;
	}

	/**
	 * 
	 * @param sh
	 */
	public static void setSurfaceHolder(SurfaceHolder sh) {
		if (callback != null)
			surfaceHolder.removeCallback(callback);
		surfaceHolder = sh;
		surfaceHolder.setKeepScreenOn(true); // zapobiega gaśnieciu ekranu
		callback = new SurfaceHolder.Callback() {
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			}

			public void surfaceCreated(SurfaceHolder holder) {
				//TODO: zrobić coś z surfaceCreated
			}

			public void surfaceDestroyed(SurfaceHolder holder) {
				if (sessionUsingTheCamera != null) {
					sessionUsingTheCamera.stopAll();
				}
			}

		};

		surfaceHolder.addCallback(callback);
	}

	/** 
	 * startuje trumień z id trackId i wysyła zdarzenie do przechwycenia w {@link StreamVideoActivity}
	 * przy {@link #startedStreamCount} = 1 
	 * 
	 * @param trackId
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public synchronized void start(int trackId) throws IllegalStateException, IOException {
		Stream stream = streamList[trackId];
		if (stream != null && !stream.isStreaming()) {
			stream.prepare();
			stream.start();
			//przy pierwszym połaczeniu właczam strumień
			if (++startedStreamCount == 1) {
				for (StreamSessionListener ssl : listeners) {
					ssl.onStart(this);
				}
			}
		}
	}
	
	/**
	 * Ustawia powierzchnię niezbędną do odpalenia {@link MediaRecorder}a
	 * @param sh {@link SurfaceHolder} 
	 * @param setACallback 
	 */
	public static void setSurfaceHolder(SurfaceHolder sh, boolean setACallback) {
		if (callback != null) surfaceHolder.removeCallback(callback);
		if (setACallback) setSurfaceHolder(sh);
		else surfaceHolder = sh;
	}
	
	/** 
	 * Ustawia miejsce docelowe
	 * @param destination The destination address
	 */
	public void setDestination(InetAddress destination) {
		this.destination =  destination;
	}
	
	/** 
	 * Ustawia sposób przesyły danych 
	 * @param routingScheme Może być Session.UNICAST lub Session.MULTICAST
	 */
	public void setRoutingScheme(int routingScheme) {
		this.routingScheme = routingScheme;
	}
	
	/** 
	 * Ustawia TTL na wszystkie pakiety podczas sesji. 
	 * Musi zostać wywołana przed dodaniem ścieżki do sesji.
	 * @param The Time To Live
	 */
	public void setTimeToLive(int ttl) {
		defaultTimeToLive = ttl;
	}
	
	/** 
	 * Dodaje kanał video 
	 * @param camera CameraInfo.CAMERA_FACING_BACK lub CameraInfo.CAMERA_FACING_FRONT
	 * @param videoQuality odpowiada za bitrate i framerate oraz rozdzielczość strumienia 
	 * @param flash prawda gdy flash ma zostać włączony
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public synchronized void addVideoTrack(boolean flash) throws IllegalStateException, IOException {
		if (sessionUsingTheCamera != null) {
//			if (sessionUsingTheCamera.routingScheme == UNICAST)
//				throw new IllegalStateException("Kamera w użyciu");
//			else {
				streamList[0] = sessionUsingTheCamera.streamList[0];
				sessionTrackCount++;
				return;
//			}
		}
		Stream stream = null;
		//standardowa jakość obrazu
		VideoQuality videoQuality = VideoQuality.merge(VideoQuality.defaultVideoQualiy, defaultVideoQuality);
		Log.d(Tags.SESSION, "Video streaming: H.263");
		stream = new H263Stream(defaultCamera);

		((VideoStream) stream).setVideoQuality(videoQuality);
		((VideoStream) stream).setPreviewDisplay(surfaceHolder.getSurface());
		((VideoStream) stream).setFlashState(flash);
		stream.setTimeToLive(defaultTimeToLive);
		stream.setDestination(destination, 5006);
		streamList[0] = stream;
		sessionUsingTheCamera = this;
		sessionTrackCount++;
	}
	
	
	/** 
	 * Returns a Session Description that can be stored in a file or sent to a client with RTSP.
	 * @return The Session Description
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public synchronized String getSessionDescription() throws IllegalStateException, IOException {
		StringBuilder sessionDescription = new StringBuilder();
		sessionDescription.append("v=0\r\n");
		// The RFC 4566 (5.2) suggest to use an NTP timestamp here but we will simply use a UNIX timestamp
		sessionDescription.append("o=- "+timestamp+" "+timestamp+" IN IP4 "+origin.getHostAddress()+"\r\n");
		sessionDescription.append("s=Unnamed\r\n");
		sessionDescription.append("i=N/A\r\n");
		sessionDescription.append("c=IN IP4 "+destination.getHostAddress()+"\r\n");
		// t=0 0 means the session is permanent (we don't know when it will stop)
		sessionDescription.append("t=0 0\r\n");
		sessionDescription.append("a=recvonly\r\n");
		// Prevents two different sessions from using the same peripheral at the same time
		for (int i=0;i<streamList.length;i++) {
			if (streamList[i] != null) {
				sessionDescription.append(streamList[i].generateSessionDescription());
				sessionDescription.append("a=control:trackID="+i+"\r\n");
			}
		}
		return sessionDescription.toString();
	}
	
	/**
	 * This method returns the selected routing scheme of the session.
	 * The routing scheme can be either Session.UNICAST or Session.MULTICAST.
	 * @return The routing sheme of the session
	 */
	public String getRoutingScheme() {
		return routingScheme==Session.UNICAST ? "unicast" : "multicast";
	}
	
	/**
	 * 
	 * @return
	 */
	public InetAddress getDestination() {
		return destination;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDestinationPort() {
		return destinationPort;
	}

	/**
	 * 
	 * @return
	 */
	public InetAddress getOrigin(){
		return origin;
	}

	/**
	 *  Returns the number of tracks of this session. 
	 * @return
	 */
	public int getTrackCount() {
		return sessionTrackCount;
	}
	
	/**
	 *  Sprawdza czy kamenta jest w użyciu
	 */
	public static boolean isCameraInUse() {
		return sessionUsingTheCamera!=null;
	}
	
	/**
	 *  Sprawdza czy mikrofon jest w użyciu
	 * @return
	 */
	public static boolean isMicrophoneInUse() {
		return sessionUsingTheMic!=null;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean trackExists(int id) {
		return streamList[id]!=null;
	}
	
	/**
	 * 
	 * @param id
	 * @param port
	 */
	public void setTrackDestinationPort(int id, int port) {
		streamList[id].setDestination(destination,port);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public int getTrackDestinationPort(int id) {
		return streamList[id].getDestinationPort();
	}
	
/**
 * 
 * @param id
 * @return
 */
	public int getTrackLocalPort(int id) {
		return streamList[id].getLocalPort();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public int getTrackSSRC(int id) {
		return streamList[id].getSSRC();
	}
	/**
	 * 
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public void startAll() throws IllegalStateException, IOException {
		for (int i = 0; i < streamList.length; i++) {
			start(i);
		}
	}

	/**
	 * 
	 */
	public synchronized void stopAll() {
		for (int i = 0; i < streamList.length; i++) {
			if (streamList[i] != null && streamList[i].isStreaming()) {
				streamList[i].stop();
				if (--startedStreamCount == 0){
					
				}
				for (StreamSessionListener ssl : listeners) {
					ssl.onStop(this);
				}
			}
		}
	}

	/**
	 * 
	 */
	public synchronized void flush() {
		for (int i = 0; i < streamList.length; i++) {
			if (streamList[i] != null) {
				streamList[i].release();
				if (i == 0)
					sessionUsingTheCamera = null;
				else
					sessionUsingTheMic = null;
			}
		}
	}
	
	/**
	 * Dodaje listenera do zbioru obiektów nasłuchujących zdarzeń z sesji
	 * @param l
	 */
	public void addListener(StreamSessionListener l){
		listeners.add(l);
	}
}
