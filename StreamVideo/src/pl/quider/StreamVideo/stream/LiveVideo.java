/**
 * 
 */
package pl.quider.StreamVideo.stream;

import java.io.IOException;

import android.media.MediaRecorder;

import pl.quider.StreamVideo.api.VideoStream;


/**
 * @author Adrian
 *
 */
public class LiveVideo extends VideoStream {

	public LiveVideo(int camera) throws IOException {
		super(camera);
		setVideoEncoder(MediaRecorder.VideoEncoder.H263);
	}

	@Override
	public String generateSessionDescription() throws IllegalStateException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
