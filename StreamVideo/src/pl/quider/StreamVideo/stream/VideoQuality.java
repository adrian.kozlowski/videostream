package pl.quider.StreamVideo.stream;
/**
 * A class that represents the quality of a video stream. 
 * It contains the resolution, the framerate (in fps) and the bitrate (in bps) of the stream.
 */
public class VideoQuality {

	/**
	 * Domyślna jakość video
	 */
	public final static VideoQuality defaultVideoQualiy = new VideoQuality(720,640,20,500000000);
	
	public int framerate = 0;
	public int bitrate = 0;
	public int resX = 0;
	public int resY = 0;
	public int orientation = 0;
	
	/**	
	 * Represents a quality for a video stream.
	 */
	public VideoQuality() {}
	
	/**
	 * Jakość wideo
	 * @param resX szerokość obrazu
	 * @param resY wysokość obrazu
	 * @param framerate fps
	 * @param bitrate bps 
	 */
	public VideoQuality(int resX, int resY, int framerate, int bitrate) {
		this.framerate = framerate;
		this.bitrate = bitrate;
		this.resX = resX;
		this.resY = resY;
	}
	
	/**
	 * 
	 * @param quality
	 * @return
	 */
	public boolean equals(VideoQuality quality) {
		if (quality==null) return false;
		return (quality.resX == this.resX 				&
				 quality.resY == this.resY 				&
				 quality.framerate == this.framerate	&
				 quality.bitrate == this.bitrate 		);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public VideoQuality clone() {
		return new VideoQuality(resX,resY,framerate,bitrate);
	}
	
	/**
	 * Parser jakości z ciągu znaków
	 * @param str ciąg znaków jakości [bitrate]-[framerate]-[x]-[y]
	 * @return
	 */
	public static VideoQuality parseQuality(String str) {
		VideoQuality quality = new VideoQuality(0,0,0,0);
		if (str != null) {
			String[] config = str.split("-");
			try {
				quality.bitrate = Integer.parseInt(config[0])*1000; 
				quality.framerate = Integer.parseInt(config[1]);
				quality.resX = Integer.parseInt(config[2]);
				quality.resY = Integer.parseInt(config[3]);
			}
			catch (IndexOutOfBoundsException ignore) {}
		}
		return quality;
	}

	/**
	 * 
	 * @param videoQuality
	 * @param withVideoQuality
	 * @return Zsumowanna jakość
	 */
	public static VideoQuality merge(VideoQuality videoQuality, VideoQuality withVideoQuality) {
		if (videoQuality.resX==0) 
			videoQuality.resX = withVideoQuality.resX;
		if (videoQuality.resY==0) 
			videoQuality.resY = withVideoQuality.resY;
		if (videoQuality.framerate==0)
			videoQuality.framerate = withVideoQuality.framerate;
		if (videoQuality.bitrate==0)
			videoQuality.bitrate = withVideoQuality.bitrate;
		return videoQuality;
	}
	
}
