/**
 * 
 */
package pl.quider.StreamVideo.properties;

/**
 * @author adrian
 *
 */
public final class Tags {

	public static final String RECORDING = "nagrywanie";
	public static final String SERVERS_ENABL = "start/stop serwera";
	public static final String MEDIA_RECORDER = "media recorder";
	public static final String SERVER_THREAD = "ServerThread";
	public static final String CLIENT = "Client";
	public static final String RTSP = "RTSP";
	public static final String CONNECTION_MANAGER = "ConnectionManager";
	public static final String RESPONSE = "Response";
	public static final String CLIENT_THREAD = "ClienThread";
	public static final String SESSION = "Session";
	public static final String REQUEST_PARSER = "RequestParser";
	public static final String DESCRIBE_RESPONSE = "DescribeResponse";
	public static final String CAMERA = "Camera";
	public static final String VIDEO_STREAM = "VideoStream";
	public static final String PACKETIZER = "H263Packetizer";

}
