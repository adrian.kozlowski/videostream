/**
 * 
 */
package pl.quider.StreamVideo.properties;




/**
 * Klasa rejestru przechowuje wszystkie dane i ustawienia
 * 
 * @author adrian
 *
 */
public final class Register {
	public static final String PREF_FILE = "StreamVideo";
	public static final String LOCAL_ADDRESS = "adresLokalny";
	public static final String REMOTE_ADDRESS = "adresZdalny";
	public static final String REMOTE_PORT = "portZdalny";
	public static final String LOCAL_PORT = "portLokalny";
	public static String QUALITY = "jakosc";
	public static String[] PROP_LIST = {
		"Port nadawania", "Jakość nadawania"
	};
}

